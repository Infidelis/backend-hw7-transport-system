from __future__ import annotations

from typing import TYPE_CHECKING

if TYPE_CHECKING:  # pragma: no cover
    from .container import Container

__all__ = ["TerminalStorage"]


class TerminalStorage:
    def __init__(self, autofind: bool = True) -> None:
        self._queue: list[Container] = []
        self.autofind = autofind

    def add(self, container: Container) -> None:
        self._queue.append(container)

    def next(self) -> Container:
        if self.is_empty():
            raise IndexError("empty queue")
        container = self._queue.pop()
        if self.autofind:
            container.navigator.find_path()
        return container

    def is_empty(self) -> bool:
        return len(self._queue) == 0

    @property
    def size(self) -> int:
        return len(self._queue)
