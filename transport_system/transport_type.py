from __future__ import annotations
from enum import Enum
from typing import Union


class TransportType(Enum):
    LORRY: str = "lorry"
    BOAT: str = "boat"

    @classmethod
    def _from_name(cls, type_: str) -> TransportType:
        try:
            return cls[type_.upper()]
        except KeyError:
            raise ValueError(f"Unkown transport type: {type_}")

    @staticmethod
    def validate_type(transport_type: Union[TransportType, str]) -> TransportType:
        if isinstance(transport_type, TransportType):
            return transport_type
        else:
            assert isinstance(transport_type, str)
            return TransportType._from_name(transport_type)
