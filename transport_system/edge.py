from __future__ import annotations

from dataclasses import dataclass
from typing import Optional, TYPE_CHECKING

from .transport_type import TransportType

if TYPE_CHECKING:  # pragma: no cover
    from .terminal import Terminal


@dataclass(frozen=True)
class Edge:
    distance: float
    start: Terminal
    finish: Terminal
    allowed_transport_type: TransportType = TransportType.LORRY

    def __post_init__(self) -> None:
        if self.distance <= 0:
            raise ValueError("Distance can't be non-positive")

        if self.start.name == self.finish.name:
            raise ValueError(
                "Invalid container initialization. "
                "Container could not be delivered to the same terminal"
            )

    def reverse(self) -> Edge:
        return Edge(start=self.finish, finish=self.start, distance=self.distance)
