from __future__ import annotations

from typing import Optional, TYPE_CHECKING

if TYPE_CHECKING:  # pragma: no cover
    from .terminal import Terminal
    from .edge import Edge

__all__ = ["Navigator"]


class Navigator:
    def __init__(self, start: Terminal, finish: Terminal):
        self.start = start
        self.finish = finish
        self._path: list[Edge] = []
        self._path_builded = False

    @staticmethod
    def _find_path(
        from_: Terminal,
        to_: Terminal,
        path: Optional[list[Edge]] = None,
        visited_terminal_names: Optional[list[str]] = None,
    ) -> Optional[list[Edge]]:
        if path is None:
            path = []
        if visited_terminal_names is None:
            visited_terminal_names = []

        if from_.name == to_.name:
            return path

        if from_.name in visited_terminal_names:
            return None

        visited_terminal_names_ = visited_terminal_names + [from_.name]
        for edge in from_.outgoing_edges:
            if found_path := Navigator._find_path(
                from_=edge.finish,
                to_=to_,
                path=path + [edge],
                visited_terminal_names=visited_terminal_names_,
            ):
                return found_path
        return None

    def find_path(self) -> None:
        _path = (
            self._find_path(self.start, self.finish) if self._path == [] else self._path
        )

        if _path is None:
            raise ValueError("No path exists!")

        self._path = _path
        self._path_builded = True

    def next(self) -> Edge:
        if self.on_place():
            raise IndexError("Container is arrived!")
        return self._path.pop(0)

    def peek(self) -> Edge:
        if self.on_place():
            raise IndexError("Container is arrived!")
        return self._path[0]

    def on_place(self) -> bool:
        if not self._path_builded:
            raise ValueError("Path has not been built")
        return len(self._path) == 0
