from __future__ import annotations

__all__ = ["Terminal"]

from typing import Optional, TYPE_CHECKING, Union

from transport_system.edge import Edge
from transport_system.terminal_storage import TerminalStorage
from .transport_type import TransportType

if TYPE_CHECKING:  # pragma: no cover
    from .transport import Transport


class Terminal:
    def __init__(self, name: str):
        self.name = name
        self.storage = TerminalStorage()
        self._from_edges: list[Edge] = []
        self._to_edges: list[Edge] = []
        self.transport: list[Transport] = []

    def __repr__(self) -> str:  # pragma: no cover
        return f"<{self.__class__.__name__} name={self.name}>"

    @property
    def incoming_edges(self) -> list[Edge]:  # pragma: no cover
        return self._to_edges

    @property
    def outgoing_edges(self) -> list[Edge]:  # pragma: no cover
        return self._from_edges

    def add_destination(
        self,
        terminal: Terminal,
        distance: float,
        allowed_transport_type: Union[str, TransportType] = TransportType.LORRY,
    ) -> None:
        edge = Edge(
            start=self,
            finish=terminal,
            distance=distance,
            allowed_transport_type=TransportType.validate_type(allowed_transport_type),
        )
        self._append_edge(edge)
        terminal._append_edge(edge)

    def _append_edge(self, edge: Edge) -> None:
        if edge.start == self:
            from_egde = edge
            to_egde = edge.reverse()

        elif edge.finish == self:
            from_egde = edge.reverse()
            to_egde = edge
        else:
            raise ValueError("Terminals are not connected")

        if from_egde not in self._from_edges:
            self._from_edges.append(from_egde)

        if to_egde not in self._to_edges:
            self._to_edges.append(to_egde)

    def is_connected(self, terminal: Terminal) -> bool:
        for from_edge in self._from_edges:
            if from_edge.finish.name == terminal.name:
                return True
        return False
