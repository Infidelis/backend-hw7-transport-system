from __future__ import annotations

from typing import Optional, Union

from transport_system.container import Container
from transport_system.edge import Edge
from transport_system.terminal import Terminal
from transport_system.transport_type import TransportType


class Transport:
    def __init__(self, type_: Union[str, TransportType]) -> None:
        self.type_ = TransportType.validate_type(type_)
        self._container: Optional[Container] = None
        self._edge: Optional[Edge] = None
        self._remains_time: Optional[float] = None

    def is_free(self) -> bool:
        return (self._container is None) and (self._remains_time is None)

    def is_arrived(self) -> bool:
        return (self._edge is not None) and (
            self._remains_time is not None and self._remains_time == 0
        )

    def unload(self) -> Optional[Container]:
        if self._remains_time != 0:
            raise ValueError("transport has not arrived yet")

        container = self._container
        self._container = None
        self._remains_time = None
        return container

    def load(self, edge: Edge, container: Optional[Container] = None) -> None:
        self._container = container
        self._edge = edge
        self._remains_time = edge.distance

    def move_back(self) -> None:
        if self._edge is None:
            raise ValueError("Empty destination")
        self.load(self._edge.reverse(), container=None)

    @property
    def destination(self) -> Terminal:
        if self._edge is None:
            raise ValueError("Empty destination")
        return self._edge.finish

    def tick(self, tick: float) -> None:
        if tick <= 0:
            raise ValueError("tick can' be negative")

        if self._remains_time is None:
            return

        self._remains_time -= tick
        if self._remains_time < 0:
            raise ValueError("remains time can't negative")
