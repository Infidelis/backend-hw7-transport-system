from typing import Optional

from transport_system.container import Container
from transport_system.terminal import Terminal
from transport_system.transport import Transport


def delivery_time_calculator(input_string: Optional[str] = None) -> int:
    if input_string is None:  # pragma: no cover
        input_string = input()

    term_S = Terminal(name="S")
    term_P = Terminal(name="P")
    term_A = Terminal(name="A")
    term_B = Terminal(name="B")

    term_S.add_destination(term_A, distance=5)
    term_S.add_destination(term_P, distance=1)
    term_P.add_destination(term_B, distance=4, allowed_transport_type="boat")

    lorry_1 = Transport(type_="lorry")
    lorry_2 = Transport(type_="lorry")
    boat = Transport(type_="boat")

    term_S.transport.append(lorry_1)
    term_S.transport.append(lorry_2)
    term_P.transport.append(boat)

    cargo_dests = input_string.split()[::-1]
    for ch in cargo_dests:
        term_S.storage.add(
            Container(reception=term_S, pick_up=term_A if ch == "A" else term_B)
        )

    terminal_list: list[Terminal] = [term_A, term_B, term_S, term_P]
    transport_list: list[Transport] = [lorry_1, lorry_2, boat]

    ticks = 0
    tick_step = 1
    while True:
        for arrived_transport in filter(Transport.is_arrived, transport_list):
            container = arrived_transport.unload()
            destination = arrived_transport.destination

            # привезли контейнер
            if container:
                destination.storage.add(container)
                arrived_transport.move_back()

        for terminal in terminal_list:
            for free_transport in filter(Transport.is_free, terminal.transport):
                if not terminal.storage.is_empty():
                    next_container = terminal.storage.next()
                    next_edge = next_container.navigator.next()
                    free_transport.load(next_edge, next_container)

        if (term_A.storage.size + term_B.storage.size) == len(cargo_dests):
            break

        for transport in transport_list:
            transport.tick(tick_step)
        ticks += tick_step
    return ticks
