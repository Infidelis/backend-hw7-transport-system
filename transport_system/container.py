from __future__ import annotations

from typing import TYPE_CHECKING

from .navigator import Navigator

if TYPE_CHECKING:  # pragma: no cover
    from .terminal import Terminal

__all__ = ["Container"]


class Container:
    reception: Terminal
    pick_up: Terminal

    def __init__(self, reception: Terminal, pick_up: Terminal) -> None:
        if reception.name == pick_up.name:
            raise ValueError(
                "Invalid container initialization. "
                "Container could not be delivered to the same terminal"
            )

        self.reception = reception
        self.pick_up = pick_up
        self.navigator = Navigator(start=self.reception, finish=self.pick_up)
