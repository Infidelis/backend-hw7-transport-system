import pytest

from transport_system.edge import Edge
from transport_system.terminal import Terminal


def test_edge_creation(term1: Terminal, term2: Terminal) -> None:
    distance = 5
    edge = Edge(start=term1, finish=term2, distance=distance)
    assert term1 == edge.start
    assert term2 == edge.finish
    assert distance == edge.distance


def test_edge_reverse(term1: Terminal, term2: Terminal) -> None:
    distance = 5
    edge = Edge(start=term1, finish=term2, distance=distance)
    reversed_edge = edge.reverse()
    assert term1 == reversed_edge.finish
    assert term2 == reversed_edge.start
    assert distance == reversed_edge.distance


def test_edge_invalid_creation_with_non_positive_distance(
    term1: Terminal, term2: Terminal
) -> None:
    with pytest.raises(ValueError):
        Edge(start=term1, finish=term2, distance=0)


def test_edge_invalid_creation_with_same_terminal(term1: Terminal) -> None:
    with pytest.raises(ValueError):
        Edge(start=term1, finish=term1, distance=1)
