import pytest

from transport_system.navigator import Navigator
from transport_system.terminal import Terminal


def test_navigator_creation(term1: Terminal, term2: Terminal) -> None:
    nav = Navigator(start=term1, finish=term2)
    assert nav.start == term1
    assert nav.finish == term2


def test_navigator_find_path_zero_edge(term1: Terminal, term2: Terminal) -> None:
    nav = Navigator(start=term1, finish=term2)
    assert nav._find_path(term1, term2) is None


def test_navigator_find_path_one_edge(term1: Terminal, term2: Terminal) -> None:
    term1.add_destination(term2, 1)

    nav = Navigator(start=term1, finish=term2)
    assert nav._find_path(term1, term2, []) == term1.outgoing_edges


def test_navigator_find_path_two_edges(
    term1: Terminal, term2: Terminal, term3: Terminal
) -> None:
    term1.add_destination(term2, 1)
    term2.add_destination(term3, 1)

    nav = Navigator(start=term1, finish=term3)
    assert nav._find_path(term1, term3) == term1.outgoing_edges + term3.incoming_edges


@pytest.fixture
def nav_1_2(term1: Terminal, term2: Terminal) -> Navigator:
    term1.add_destination(term2, 1)
    return Navigator(start=term1, finish=term2)


def test_navigator_on_place_empty(nav_1_2: Navigator) -> None:
    with pytest.raises(ValueError):
        nav_1_2.on_place()


def test_navigator_on_place_not_arived(nav_1_2: Navigator) -> None:
    nav_1_2.find_path()
    assert not nav_1_2.on_place()


def test_navigator_next_empty(nav_1_2: Navigator) -> None:
    with pytest.raises(ValueError):
        nav_1_2.next()


def test_navigator_peek_empty(nav_1_2: Navigator) -> None:
    with pytest.raises(ValueError):
        nav_1_2.peek()


def test_navigator_next(nav_1_2: Navigator) -> None:
    nav_1_2.find_path()
    assert nav_1_2.next() == nav_1_2.start.outgoing_edges[0]
    assert nav_1_2.on_place()

    with pytest.raises(IndexError):
        nav_1_2.next()


def test_navigator_peek(nav_1_2: Navigator) -> None:
    nav_1_2.find_path()
    assert nav_1_2.peek() == nav_1_2.start.outgoing_edges[0]
    assert nav_1_2.peek() == nav_1_2.start.outgoing_edges[0]
    nav_1_2.next()
    with pytest.raises(IndexError):
        nav_1_2.peek()


def test_navigator_find_path_empty_path(term1: Terminal, term2: Terminal) -> None:
    nav = Navigator(start=term1, finish=term2)
    with pytest.raises(ValueError):
        nav.find_path()
