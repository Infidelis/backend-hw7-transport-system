import pytest

from transport_system.terminal import Terminal
from transport_system.container import Container


def test_container_creation(term1: Terminal, term2: Terminal) -> None:
    container = Container(reception=term1, pick_up=term2)
    assert term1 == container.reception
    assert term2 == container.pick_up


def test_container_invalid_creation_with_same_terminal(term1: Terminal) -> None:
    with pytest.raises(ValueError):
        Container(reception=term1, pick_up=term1)
