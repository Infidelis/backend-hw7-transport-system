import pytest

from transport_system.container import Container
from transport_system.edge import Edge
from transport_system.transport import Transport
from transport_system.transport_type import TransportType


def test_transport_type_validation() -> None:
    lorry_type = TransportType.LORRY
    assert TransportType.validate_type("lorry") == lorry_type
    assert TransportType.validate_type(lorry_type) == lorry_type


def test_transport_type_validation_with_unknown_type() -> None:
    with pytest.raises(ValueError):
        TransportType.validate_type("lorry1")


def test_transport_creation() -> None:
    transport = Transport("lorry")
    assert transport.type_ == TransportType.LORRY


def test_transport_is_free(
    lorry: Transport, term1_term2_edge: Edge, container1: Container
) -> None:
    assert lorry.is_free()
    lorry.load(term1_term2_edge, container1)
    assert not lorry.is_free()


def test_transport_destination(
    lorry: Transport, term1_term2_edge: Edge, container1: Container
) -> None:
    lorry.load(term1_term2_edge, container1)
    assert lorry.destination == term1_term2_edge.finish


def test_transport_empty_unload(lorry: Transport) -> None:
    with pytest.raises(ValueError):
        lorry.unload()


def test_transport_empty_destination(lorry: Transport) -> None:
    with pytest.raises(ValueError):
        _ = lorry.destination


def test_transport_unload_not_arrived(
    lorry: Transport, term1_term2_edge: Edge, container1: Container
) -> None:
    lorry.load(term1_term2_edge, container1)
    assert not lorry.is_arrived()
    with pytest.raises(ValueError):
        lorry.unload()


def test_transport_unload(
    lorry: Transport, term1_term2_edge: Edge, container1: Container
) -> None:
    lorry.load(term1_term2_edge, container1)
    assert not lorry.is_arrived()
    lorry.tick(term1_term2_edge.distance)
    assert lorry.is_arrived()
    assert lorry.unload() == container1
    assert lorry.is_free()


def test_transport_non_negative_tick(
    lorry: Transport, term1_term2_edge: Edge, container1: Container
) -> None:
    lorry.load(term1_term2_edge, container1)
    with pytest.raises(ValueError):
        lorry.tick(-1)


def test_transport_move_back(
    lorry: Transport, term1_term2_edge: Edge, container1: Container
) -> None:
    with pytest.raises(ValueError):
        lorry.move_back()


def test_transport_negative_tick(
    lorry: Transport, term1_term2_edge: Edge, container1: Container
) -> None:
    lorry.load(term1_term2_edge, container1)
    with pytest.raises(ValueError):
        lorry.tick(term1_term2_edge.distance + 1)
