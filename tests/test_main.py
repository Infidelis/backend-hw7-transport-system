from transport_system import delivery_time_calculator


def test_delivery_time_calculator_1() -> None:
    assert delivery_time_calculator("A") == 5


def test_delivery_time_calculator_2() -> None:
    assert delivery_time_calculator("B") == 5


def test_delivery_time_calculator_3() -> None:
    assert delivery_time_calculator("A B") == 5


def test_delivery_time_calculator_4() -> None:
    assert delivery_time_calculator("B A") == 5


def test_delivery_time_calculator_5() -> None:
    assert delivery_time_calculator("B A A") == 7


def test_delivery_time_calculator_6() -> None:
    assert delivery_time_calculator("B B A B A A B A") == 29


def test_delivery_time_calculator_7() -> None:
    assert delivery_time_calculator("B A A A B A B B A A A") == 39
