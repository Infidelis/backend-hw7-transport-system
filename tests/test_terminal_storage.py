import pytest

from transport_system.container import Container
from transport_system.terminal_storage import TerminalStorage


def test_terminal_storage_is_empty_queue(
    storage: TerminalStorage, container1: Container
) -> None:
    assert storage.is_empty()
    storage.add(container1)
    assert not storage.is_empty()


def test_terminal_storage_next_empty(storage: TerminalStorage) -> None:
    with pytest.raises(IndexError):
        storage.next()


def test_terminal_storage_next(
    storage: TerminalStorage, container1: Container, container2: Container
) -> None:
    storage.add(container1)
    storage.add(container2)
    assert storage.next() == container2
    assert storage.size == 1
