import pytest

from transport_system.edge import Edge
from transport_system.terminal import Terminal


def test_terminal_add_edges_with_two_add(term1: Terminal, term2: Terminal) -> None:
    edge = Edge(start=term1, finish=term2, distance=1)
    term1._append_edge(edge)
    term1._append_edge(edge.reverse())

    assert len(term1._from_edges) == 1
    assert len(term1._to_edges) == 1


def test_terminal_add_edges_with_separate_terminal_add(
    term1: Terminal, term2: Terminal
) -> None:
    edge = Edge(start=term1, finish=term2, distance=1)
    term1._append_edge(edge)
    term2._append_edge(edge)

    assert len(term1._from_edges) == 1
    assert len(term2._to_edges) == 1


def test_terminal_add_edgesd(term1: Terminal, term2: Terminal) -> None:
    term1.add_destination(term2, 1)

    assert len(term1._from_edges) == 1
    assert len(term2._to_edges) == 1


def test_terminal_append_not_connected_edge(
    term1: Terminal, term2: Terminal, term3: Terminal
) -> None:
    with pytest.raises(ValueError):
        term1._append_edge(Edge(start=term2, finish=term3, distance=1))


def test_terminal_is_connected(term1: Terminal, term2: Terminal) -> None:
    term1.add_destination(term2, 1)
    assert term1.is_connected(term2)
    assert term2.is_connected(term1)


def test_terminal_is_connected_empty(term1: Terminal, term2: Terminal) -> None:
    assert not term1.is_connected(term2)


def test_terminal_is_not_connected_with_other(
    term1: Terminal, term2: Terminal, term3: Terminal
) -> None:
    term1.add_destination(term2, 1)
    assert not term1.is_connected(term3)
