import pytest

from transport_system.container import Container
from transport_system.edge import Edge
from transport_system.terminal import Terminal
from transport_system.terminal_storage import TerminalStorage
from transport_system.transport import Transport


@pytest.fixture()
def term1() -> Terminal:
    return Terminal("1")


@pytest.fixture()
def term2() -> Terminal:
    return Terminal("2")


@pytest.fixture()
def term3() -> Terminal:
    return Terminal("3")


@pytest.fixture()
def container1(term1: Terminal, term2: Terminal) -> Container:
    return Container(reception=term1, pick_up=term2)


@pytest.fixture()
def container2(term1: Terminal, term3: Terminal) -> Container:
    return Container(reception=term1, pick_up=term3)


@pytest.fixture()
def storage() -> TerminalStorage:
    return TerminalStorage(autofind=False)


@pytest.fixture()
def term1_term2_edge(term1: Terminal, term2: Terminal) -> Edge:
    return Edge(start=term1, finish=term2, distance=1)


@pytest.fixture()
def lorry() -> Transport:
    return Transport("lorry")


@pytest.fixture()
def boat() -> Transport:
    return Transport("boat")
