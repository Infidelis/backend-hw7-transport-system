# Транспортная система

## Ответ программы на задание

```
B B A B A A B A ==> 29
B A A A B A B B A A A ==> 39
```


## Начало работы

```
pip3 install <путь к бинарнику>
```

Далее
```
python3 -m transport_system
```

и вводим строку в консоль

## Разработка

В проекте используется:

1. Линтер для статического типизирования - [MyPy](https://mypy.readthedocs.io/en/stable/)
2. Линтер для стиля - [black](https://github.com/psf/black)
3. Сборка и установка - [poetry](https://python-poetry.org/)
4. Измерение покрытия тестами - [pytest-cov](https://pytest-cov.readthedocs.io/en/latest/)



Чтобы установить зависимости и развенуть окружение
(нужны установленные `python3.9` и `poetry`):

```bash
make install
```


Запуск тестов (также считает покрытие):

```bash
make test
```


Проверка линтерами (`mypy` - типизация, `black` - стиль):

```bash
make lint
```


Форматирование кода линтером `black`:

```bash
make format
```


Генерация `python` пакета:

```bash
make build
```


Очистка от cache файлов:

```bash
make clean
```
